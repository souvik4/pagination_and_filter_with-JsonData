import './App.css'
import Searching from './Searching/Searching';

const App  = () => {
  return(
    <div>
      <Searching />
    </div>
  )
}

export default App;