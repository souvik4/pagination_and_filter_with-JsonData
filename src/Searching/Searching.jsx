import JsonData from '../db.json'
import { useState } from 'react';
import ReactPaginate from 'react-paginate';

const Searching = () => {
    const [searchString, setSearchString]  = useState('');
    const [users, setUsers]  = useState(JsonData.slice(0,100));
    const [pageNumber, setPageNumber] = useState(0)

    const usersPerPage = 10
    const pagesVisited = pageNumber * usersPerPage;

    const displayUsers = users.slice(pagesVisited, pagesVisited + usersPerPage)
    .filter(val=> {
      if(searchString === ''){
        return val;
      }else if(val.first_name.toLocaleLowerCase().includes(searchString.toLocaleLowerCase())){
        return val;
      }
    }).map((item, key) => {
      return(
        <div className='user' key={key}>
          <div>
            <p>{item.id + ". " +item.first_name+"  "+ item.last_name}</p>
          </div>
        </div>
      )
    })
 
  const handlePageClick = ({selected}) => {
    setPageNumber(selected);
  }

  const pageCount = Math.ceil(users.length / usersPerPage)
  

  return (
    <div className="App">

      <input 
        type='search' 
        placeholder='Search-Box'
        onChange={(event) => {
          setSearchString(event.target.value);
          console.log(event.target.value)
        }}
      />
      {displayUsers}

      

    <ReactPaginate
        
        breakLabel="..."
        nextLabel="next >"
        pageCount={pageCount}
        pageRangeDisplayed={2}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        onPageChange={handlePageClick}
        containerClassName={'pagination justify-content-center'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        previousClassName={'page-item'}
        previousLinkClassName={'page-link'}
        nextClassName={'page-item'}
        nextLinkClassName={'page-link'}
        breakClassName={'page-item'}
        breakLinkClassName={'page-link'}
        activeClassName={'active'}
      />
    
    </div>
  );
}

export default Searching;